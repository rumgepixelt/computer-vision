import csv
import os
import cv2
from utils.download_helper import download_with_progress
from yolo3.train import yolo_train
from yolo3.yolo import YOLO
import copy
from PIL import Image


class OpenImageDataHelper:
    # define were your figure eight data is stored
    DEFAULT_DATA_PATH = "./data"
    # LINKS DOWNLOAD INFOS and ANNOTATIONS
    CLASS_DEFINITION_FILE = "class-descriptions-boxable.csv"
    CLASS_DEFINITION_LINK = "https://storage.googleapis.com/openimages/v5/class-descriptions-boxable.csv"

    VALIDATION_ANNOTATION_FILE = "validation-annotations-bbox.csv"
    VALIDATION_ANNOTATION_LINK = "https://storage.googleapis.com/openimages/v5/validation-annotations-bbox.csv"
    VALIDATION_SINGLE_IMAGE_DOWNLOAD_PATH_FILE = "validation-images.csv"
    VALIDATION_SINGLE_IMAGE_DOWNLOAD_PATH_LINK = "https://datasets.figure-eight.com/figure_eight_datasets/open-images/validation-images.csv"

    TEST_ANNOTATION_FILE = "test-annotations-bbox.csv"
    TEST_ANNOTATION_LINK = "https://storage.googleapis.com/openimages/v5/test-annotations-bbox.csv"
    TEST_SINGLE_IMAGE_DOWNLOAD_PATH_FILE = "test-images.csv"
    TEST_SINGLE_IMAGE_DOWNLOAD_PATH_LINK = "https://datasets.figure-eight.com/figure_eight_datasets/open-images/test-images.csv"

    TRAINING_ANNOTATION_FILE = "train-annotations-bbox.csv"
    TRAINING_ANNOTATION_LINK = "https://datasets.figure-eight.com/figure_eight_datasets/open-images/train-annotations-bbox.csv"
    TRAINING_SINGLE_IMAGE_DOWNLOAD_PATH_FILE = "train-images-boxable.csv"
    TRAINING_SINGLE_IMAGE_DOWNLOAD_PATH_LINK = "https://datasets.figure-eight.com/figure_eight_datasets/open-images/train-images-boxable.csv"

    # Images ZIP files baseurl
    IMAGES_ZIP_BASE_URL = "https://datasets.figure-eight.com/figure_eight_datasets/open-images/zip_files_copy/"  # + train_00.zip .....train_08.zip | validation.zip | test.zip

    def __init__(self, data_root_dir=DEFAULT_DATA_PATH, classes_for_training=None):
        self.data_root_dir = data_root_dir
        self.classes = list()
        self.classes_keys = self.__read_classes()
        self.validation_anno = self.__read_val_annos()
        self.test_anno = self.__read_test_annos()

        self.train_anno = self.__read_train_annos()
        self.train_dirs = self.__get_all_train_dirs()

    def eval_model_OLD(self, test_data_path, classes_path, yolo, show_images=False, save_images=False):
        def bb_intersection_over_union(boxA, boxB):
            # determine the (x, y)-coordinates of the intersection rectangle
            xA = max(boxA[0], boxB[0])  # left
            yA = max(boxA[1], boxB[1])  # top
            xB = min(boxA[2], boxB[2])  # right
            yB = min(boxA[3], boxB[3])  # bottom

            # compute the area of intersection rectangle
            interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

            # compute the area of both the prediction and ground-truth
            # rectangles
            boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
            boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

            # compute the intersection over union by taking the intersection
            # area and dividing it by the sum of prediction + ground-truth
            # areas - the interesection area
            iou = interArea / float(boxAArea + boxBArea - interArea)

            # return the intersection over union value
            return iou

        min_iou = 0.5

        y = yolo
        with open(classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        print(class_names)
        given_test_annos = dict()
        for image_id in self.test_anno.keys():
            new_annotation = list()
            for a in self.test_anno[image_id]:
                if a[0] in class_names:
                    new_annotation.append(a)
            if len(new_annotation) > 0:
                given_test_annos[image_id] = new_annotation
        print("Lenght: " + str(len(given_test_annos.keys())))
        all_existing_lables = 0
        all_detected_lables = 0
        all_matched_lables = 0
        current_test_image_num = 0
        perfect_match_num = 0
        precision_sum = 0
        sensitivity_sum = 0
        for image_id in given_test_annos.keys():
            image_path = test_data_path + "/" + image_id + ".jpg"
            image = Image.open(image_path)
            r_image, detect_annotations, scores = y.detect_classes(image)
            if show_images:
                try:
                    r_image = y.detect_image(image)
                except:
                    print("error")
            width = image.width
            height = image.height
            positives_num = len(given_test_annos[image_id])
            detected_label_num = len(detect_annotations)
            matched_labels = list()
            for anno in given_test_annos[image_id]:
                for i in range(0, len(detect_annotations)):
                    if anno[0] == detect_annotations[i][0] and anno[0] in class_names:
                        bboxA = (int(float(anno[1]) * width), int(float(anno[3]) * height), int(float(anno[2]) * width),
                                 int(float(anno[4]) * height))
                        bboxB = (detect_annotations[i][1], detect_annotations[i][3], detect_annotations[i][2],
                                 detect_annotations[i][4])
                        ui = bb_intersection_over_union(bboxA, bboxB)
                        if ui >= min_iou:
                            matched_labels.append(detect_annotations[i])
            # sensitivity(recall) --> tpr=tp/p
            # precision(positive predictive value) --> ppv = tp/(tp+fp)
            # f1 score
            true_positive_num = len(matched_labels)
            false_positves_num = detected_label_num - true_positive_num
            false_negative_num = positives_num - true_positive_num
            sensitivity = true_positive_num / positives_num
            if true_positive_num + false_positves_num == 0:
                precision = 0
            else:
                precision = true_positive_num / (true_positive_num + false_positves_num)
            sensitivity_sum += sensitivity
            precision_sum += precision

            all_existing_lables += positives_num
            all_detected_lables += detected_label_num
            all_matched_lables += true_positive_num
            current_test_image_num += 1
            if true_positive_num == positives_num:
                perfect_match_num += 1
            if current_test_image_num % 100 == 0:
                print("------------")
                print("current_images: " + str(current_test_image_num) + " / " + str(len(given_test_annos.keys())))
                print("precision:" + str(precision_sum / current_test_image_num))
                print("sensitivity:" + str(sensitivity_sum / current_test_image_num))
                print("perfect_match:" + str(perfect_match_num))

            if show_images:
                print("Matched:", str(true_positive_num))
                print(matched_labels)
                print("Detected:", str(detected_label_num))
                print(detect_annotations)
                print("Grounding Truth:", str(positives_num))
                try:
                    if not save_images:
                        r_image.show()
                    image_grounded_truth = show_images_with_bboxes(image_path, given_test_annos[image_id],
                                                                   save_image=save_images)

                    origin_img = "../data/images_with_labels/" + image_path.split("/")[-1].split(".")[0] + "_ORIGIN.jpg"
                    detected_img = "../data/images_with_labels/" + image_path.split("/")[-1].split(".")[
                        0] + "_DETECT.jpg"
                    print(origin_img)
                    cv2.imwrite(origin_img, image_grounded_truth)
                    r_image.save(detected_img)
                except:
                    print("error")

            # show_images_with_bboxes(image_path, self.test_anno[image_id])
        print("Matched:", str(all_matched_lables))
        print("Detected:", str(all_detected_lables))
        print("All existing:", str(all_existing_lables))
        y.close_session()

    def eval_model_with_map_tool(self, test_data_path, classes_path, yolo, save_images=False, max_images=0):
        DIR_TO_MAP_INPUT = os.path.dirname(__file__)
        DIR_TO_MAP_INPUT = os.path.join(DIR_TO_MAP_INPUT, '../eval/mAP-master/input')
        if not os.path.exists(DIR_TO_MAP_INPUT):
            os.mkdir(DIR_TO_MAP_INPUT)
        print(DIR_TO_MAP_INPUT)
        DIR_TO_DETECTION_RESULT = DIR_TO_MAP_INPUT + "/detection-results"
        DIR_TO_GROUND_TRUTH = DIR_TO_MAP_INPUT + "/ground-truth"
        DIR_IMAGES_OPTIONAL = DIR_TO_MAP_INPUT + "/images-optional"
        y = yolo
        with open(classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        print(class_names)
        given_test_annos = dict()
        for image_id in self.test_anno.keys():
            new_annotation = list()
            for a in self.test_anno[image_id]:
                if a[0] in class_names:
                    new_annotation.append(a)
            if len(new_annotation) > 0:
                given_test_annos[image_id] = new_annotation
        print("Lenght: " + str(len(given_test_annos.keys())))
        image_count = 0
        for image_id in given_test_annos.keys():
            image_path = test_data_path + "/" + image_id + ".jpg"
            image = Image.open(image_path)
            r_image, detect_annotations, scores = y.detect_classes(image)
            # print(detect_annotations, scores)
            # print(given_test_annos[image_id])
            detect_result_img_txt = open(os.path.abspath(DIR_TO_DETECTION_RESULT + "/" + image_id + ".txt"), 'w+')
            ground_truth_img_txt = open(os.path.abspath(DIR_TO_GROUND_TRUTH + "/" + image_id + ".txt"), 'w+')
            if save_images:
                image.save(DIR_IMAGES_OPTIONAL + "/" + image_id + ".jpg")
            width = image.width
            height = image.height
            for anno in given_test_annos[image_id]:
                bboxA = (int(float(anno[1]) * width), int(float(anno[3]) * height), int(float(anno[2]) * width),
                         int(float(anno[4]) * height))
                ground_truth_img_txt.write(anno[0].replace(" ", "_"))
                for c in bboxA:
                    ground_truth_img_txt.write(" " + str(c))
                ground_truth_img_txt.write("\n")
            for i in range(0, len(detect_annotations)):
                bboxB = (detect_annotations[i][1], detect_annotations[i][3], detect_annotations[i][2],
                         detect_annotations[i][4])
                detect_result_img_txt.write(detect_annotations[i][0].replace(" ", "_"))
                detect_result_img_txt.write(" " + str(scores[i]))
                for c in bboxB:
                    detect_result_img_txt.write(" " + str(c))
                detect_result_img_txt.write("\n")
            detect_result_img_txt.close()
            ground_truth_img_txt.close()
            image_count += 1
            if max_images > 0:
                if image_count > max_images:
                    break
            if image_count % 100 == 0:
                print(str(image_count), "done")
        y.close_session()

    def setup_training(self, classes, max_image_num, session_name):
        classes_dict = dict()
        class_id = 0
        class_set = set()
        images = dict()
        images_list = list()
        for c in classes:
            classes_dict[c] = [class_id, 0, list()]
            class_id += 1

        class_set.update(classes)
        first_letters = list()
        for d in self.train_dirs:
            first_letters.append(d[-1:])
        print(first_letters)
        for image_id in self.train_anno.keys():
            for label in self.train_anno[image_id]:
                if label[0] in class_set:
                    if classes_dict[label[0]][1] <= max_image_num:
                        for t_dir in self.train_dirs:
                            path = self.data_root_dir + "/" + t_dir + "/" + image_id + ".jpg"
                            if os.path.exists(path) and image_id not in images.keys():
                                classes_dict[label[0]][1] += 1
                                images[image_id] = self.train_anno[image_id]
                                line = self.__generate_annotation_line(image_id, path, classes_dict)
                                images_list.append(line)
                    else:
                        class_set.remove(label[0])
                        print(label[0], "hat maximum erreicht")
                        classes_dict[label[0]][1] += 1
            if len(class_set) == 0:
                print("klassen sind durch")
                break
        print(class_set)
        for element in class_set:
            print(classes_dict[element])
        output_path = self.data_root_dir + "/training"
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        output_path += "/" + session_name
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        output_path += "/output"
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        output_file = output_path + "/train.txt"
        with open(output_file, 'w') as annotation_file:
            for image in images_list:
                annotation_file.write(image + "\n")
        output_file_classes = output_path + "/classes.txt"
        with open(output_file_classes, 'w') as class_file:
            for c in classes:
                class_file.write(c + "\n")
        return images, images_list

    def setup_training_body_parts(self, classes, max_image_num, session_name):
        classes_dict = dict()
        class_id = 0
        class_set = set()
        images = dict()
        images_list = list()
        for c in classes:
            classes_dict[c] = [class_id, 0, list()]
            class_id += 1

        class_set.update(classes)
        first_letters = list()
        for d in self.train_dirs:
            first_letters.append(d[-1:])
        #load specific annos
        ids = list()
        f = open("/home/zulhakar/PycharmProjects/computer-vision/yolo_training/yolo3/model_data/train-image-ids-with-human-parts-and-mammal-boxes.txt", "r")
        for x in f:
            ids.append(x[:-1])
        print(first_letters)
        for image_id in ids:
            for label in self.train_anno[image_id]:
                if label[0] in class_set:
                    if classes_dict[label[0]][1] <= max_image_num:
                        for t_dir in self.train_dirs:
                            path = self.data_root_dir + "/" + t_dir + "/" + image_id + ".jpg"
                            if os.path.exists(path) and image_id not in images.keys():
                                classes_dict[label[0]][1] += 1
                                images[image_id] = self.train_anno[image_id]
                                line = self.__generate_annotation_line(image_id, path, classes_dict)
                                images_list.append(line)
                    else:
                        class_set.remove(label[0])
                        print(label[0], "hat maximum erreicht")
                        classes_dict[label[0]][1] += 1
            if len(class_set) == 0:
                print("klassen sind durch")
                break
        print(class_set)
        for element in class_set:
            print(classes_dict[element])
        output_path = self.data_root_dir + "/training"
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        output_path += "/" + session_name
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        output_path += "/output"
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        output_file = output_path + "/train.txt"
        with open(output_file, 'w') as annotation_file:
            for image in images_list:
                annotation_file.write(image + "\n")
        output_file_classes = output_path + "/classes.txt"
        with open(output_file_classes, 'w') as class_file:
            for c in classes:
                class_file.write(c + "\n")
        return images, images_list


    # XMin,XMax,YMin,YMax  -> google_data format
    # x_min,y_min,x_max,y_max,class_id  --> yolo yolo_training
    # cv2.rectangle(img, (int(float(a[1]) * width), int(float(a[3]) * height)),
    #               (int(float(a[2]) * width), int(float(a[4]) * height)), (255, 0, 0), 2)

    def __generate_annotation_line(self, image_id, path, classes_dict):
        annotation = self.train_anno[image_id]
        line = path
        img = cv2.imread(path)
        height, width, channels = img.shape
        for a in annotation:
            if a[0] in classes_dict.keys():
                line += " "
                x_min = int(float(a[1]) * width)
                y_min = int(float(a[3]) * height)
                x_max = int(float(a[2]) * width)
                y_max = int(float(a[4]) * height)
                class_name = a[0]
                line += str(x_min) + ","
                line += str(y_min) + ","
                line += str(x_max) + ","
                line += str(y_max) + ","
                line += str(classes_dict[class_name][0])
        return line

    def __get_file_path(self, filename):
        path = self.data_root_dir + "/" + filename
        if os.path.exists(path):
            return path
        else:
            path = self.DEFAULT_DATA_PATH + "/" + filename
            if os.path.exists(path):
                return path
            else:
                return None

    def __read_classes(self):
        class_file = self.__get_file_path(self.CLASS_DEFINITION_FILE)
        if class_file is None:
            if not os.path.exists(self.DEFAULT_DATA_PATH):
                os.mkdir(self.DEFAULT_DATA_PATH)
            download_with_progress(self.CLASS_DEFINITION_LINK, self.CLASS_DEFINITION_FILE, self.data_root_dir)
            class_file = self.__get_file_path(self.CLASS_DEFINITION_FILE)
        class_dict = dict()
        try:
            with open(class_file, 'r') as csvfile:
                reader = csv.reader(csvfile)
                for row in reader:
                    class_dict[row[0]] = row[1]
                print("Anzahl an klassen: ", str(len(class_dict.keys())))
                self.classes = list(class_dict.values())
                self.classes.sort()
                print(self.classes)
                return class_dict
        except:
            print("Die Figure Eight class-descriptions-boxable.csv file wurde nicht gefunden")
            return None

    def __read_val_annos(self):
        anno_file = self.__get_file_path(self.VALIDATION_ANNOTATION_FILE)
        if anno_file is None:
            download_with_progress(self.VALIDATION_ANNOTATION_LINK, self.VALIDATION_ANNOTATION_FILE, self.data_root_dir)
            anno_file = self.__get_file_path(self.VALIDATION_ANNOTATION_FILE)
        return self.__read_anno(anno_file)

    def __read_test_annos(self):
        anno_file = self.__get_file_path(self.TEST_ANNOTATION_FILE)
        if anno_file is None:
            download_with_progress(self.TEST_ANNOTATION_LINK, self.TEST_ANNOTATION_FILE, self.data_root_dir)
            anno_file = self.__get_file_path(self.TEST_ANNOTATION_FILE)
        return self.__read_anno(anno_file)

    def __read_train_annos(self):
        anno_file = self.__get_file_path(self.TRAINING_ANNOTATION_FILE)
        if anno_file is None:
            download_with_progress(self.TRAINING_ANNOTATION_LINK, self.TRAINING_ANNOTATION_FILE, self.data_root_dir)
            anno_file = self.__get_file_path(self.TRAINING_ANNOTATION_FILE)
        return self.__read_anno(anno_file)

    def __read_anno(self, annotation_file):
        print("Parsing...", annotation_file)
        annotation_dict = dict()
        anno_counter = 0
        with open(annotation_file, 'r') as csvfile:
            reader = csv.reader(csvfile)
            headers = next(reader)
            # 'ImageID', 'Source', 'LabelName', 'Confidence', 'XMin', 'XMax', 'YMin', 'YMax', 'IsOccluded', 'IsTruncated', 'IsGroupOf', 'IsDepiction', 'IsInside'
            for row in reader:
                anno_counter += 1
                image_id = row[0]
                if len(image_id) < 16:
                    print(image_id)
                if image_id not in annotation_dict.keys():
                    annotation_dict[image_id] = list()
                annotation_dict[image_id].append((self.classes_keys[row[2]], row[4], row[5], row[6], row[7]))
        # KEY: ImageID, VALUE: LABLENAME, XMin, XMax, YMin, YMax,
        print("Anzahl an label:", str(anno_counter))
        print("Anzahl an Pictures:", str(len(annotation_dict.keys())))
        return annotation_dict

    def __get_all_train_dirs(self):
        print("Check training, test, validation data... ")
        dirs = os.listdir(self.data_root_dir)
        train_dirs = list()
        for d in dirs:
            if os.path.isdir(self.data_root_dir + "/" + d):
                if "train_" in d:
                    print("Found", d)
                    train_dirs.append(d)
                if "validation" in d:
                    print("Found", d)
                if "test" in d:
                    print("Found", d)
        return train_dirs


def show_images_with_bboxes(file_path, annotation, waiting_time=0, save_image=False):
    img = cv2.imread(file_path)
    height, width, channels = img.shape

    for a in annotation:
        print(a)
        cv2.rectangle(img, (int(float(a[1]) * width), int(float(a[3]) * height)),
                      (int(float(a[2]) * width), int(float(a[4]) * height)), (255, 0, 0), 2)
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.rectangle(img, (int(float(a[1]) * width), int(float(a[3]) * height) - 20),
                      (int(float(a[2]) * width), int(float(a[3]) * height)), (255, 0, 0), cv2.FILLED)
        cv2.putText(img, a[0], (int(float(a[1]) * width), int(float(a[3]) * height)), font, 1, (255, 255, 255), 1,
                    cv2.LINE_AA)

    if not save_image:
        cv2.imshow(file_path, img)
        cv2.waitKey(waiting_time)
        cv2.destroyAllWindows()
    return img
    # XMin,XMax,YMin,YMax  -> google_data format
    # x_min,y_min,x_max,y_max,class_id  --> yolo yolo_training


def show_images_with_bboxes3(file_path, annotation, waiting_time=0):
    img = cv2.imread(file_path)
    for a in annotation:
        print(a)
        cv2.rectangle(img, (int(float(a[1])), int(float(a[3]))),
                      (int(float(a[2])), int(float(a[4]))), (255, 0, 0), 2)
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img, a[0], (int(float(a[1])), int(float(a[3]))), font, 1, (255, 255, 255), 1,
                    cv2.LINE_AA)

    cv2.imshow(file_path, img)
    cv2.waitKey(waiting_time)
    cv2.destroyAllWindows()


def show_images_with_bboxes2(line, waiting_time=0):
    splited_line = line.split(" ")
    path = splited_line[0]
    img = cv2.imread(path)

    for box in splited_line[1:]:
        print(box)
        b_d = box.split(",")
        cv2.rectangle(img, (int(b_d[0]), int(b_d[1])),
                      (int(b_d[2]), int(b_d[3])), (255, 0, 0), 2)
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img, b_d[4], (int(b_d[0]), int(b_d[1])), font, 1, (255, 255, 255), 1,
                    cv2.LINE_AA)

    cv2.imshow(path, img)
    cv2.waitKey(waiting_time)
    cv2.destroyAllWindows()


def show_train_images(train_txt_file):
    with open(train_txt_file) as file:
        content = file.readlines()
    content = [x.strip() for x in content]
    for image in content:
        show_images_with_bboxes2(image, 2000)


def training_with_80_coco_classes(path_to_training_data, session_name, max_images_per_class=110, input_shape=(416, 416),
                                  batchsize=32, setup_train=True, training=True, epochs=20):
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    session_name += "_im_" + str(max_images_per_class) + "_shape_" + str(input_shape[0]) + "_batch_" + str(batchsize)
    if setup_train:
        with open(ROOT_DIR + "/yolo3/model_data/coco_names_to_google.txt", "r") as coco_classes_file:
            class_names = coco_classes_file.readlines()
        class_names = [c.strip() for c in class_names]
        print(class_names)
        f = OpenImageDataHelper(path_to_training_data)
        f.setup_training(class_names, max_images_per_class, session_name)
    if training:
        weights_file = ROOT_DIR + '/yolo3/model_data/yolo.h5'
        annotation_path = path_to_training_data + '/training/' + session_name + '/output/train.txt'
        log_dir = path_to_training_data + '/training/' + session_name + '/output'
        classes_path = path_to_training_data + '/training/' + session_name + '/output/classes.txt'
        anchors_path = ROOT_DIR + '/yolo3/model_data/yolo_anchors.txt'
        yolo_train(annotation_path, log_dir, classes_path, anchors_path, weights_file, batchsize=batchsize,
                   epochs=epochs,
                   input_shape=input_shape)

def training_with_person_body_parts(path_to_training_data, session_name, max_images_per_class=110, input_shape=(416, 416),
                                  batchsize=32, setup_train=True, training=True, epochs=20):
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    session_name += "_im_" + str(max_images_per_class) + "_shape_" + str(input_shape[0]) + "_batch_" + str(batchsize)
    if setup_train:

        class_names = ["Man", "Woman"]
        print(class_names)
        f = OpenImageDataHelper(path_to_training_data)
        f.setup_training_body_parts(class_names, max_images_per_class, session_name)
    if training:
        weights_file = ROOT_DIR + '/yolo3/model_data/yolo.h5'
        annotation_path = path_to_training_data + '/training/' + session_name + '/output/train.txt'
        log_dir = path_to_training_data + '/training/' + session_name + '/output'
        classes_path = path_to_training_data + '/training/' + session_name + '/output/classes.txt'
        anchors_path = ROOT_DIR + '/yolo3/model_data/yolo_anchors.txt'
        yolo_train(annotation_path, log_dir, classes_path, anchors_path, weights_file, batchsize=batchsize,
                   epochs=epochs,
                   input_shape=input_shape)


def training_with_2_classes(path_to_training_data, session_name, setup_train=True, training=True):
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    if setup_train:
        class_names = ["Man", "Woman"]
        print(class_names)
        f = OpenImageDataHelper(path_to_training_data)
        f.setup_training(class_names, 110, session_name)
    if training:
        weights_file = ROOT_DIR + '/yolo3/model_data/yolo.h5'
        annotation_path = path_to_training_data + '/training/' + session_name + '/output/train.txt'
        log_dir = path_to_training_data + '/training/' + session_name + '/output'
        classes_path = path_to_training_data + '/training/' + session_name + '/output/classes.txt'
        anchors_path = ROOT_DIR + '/yolo3/model_data/yolo_anchors.txt'
        yolo_train(annotation_path, log_dir, classes_path, anchors_path, weights_file, batchsize=32, epochs=1)


def eval_custom_test(session_name, data_path, h5_file):
    model_path = data_path + '/training/' + session_name + "/output/" + h5_file
    classes_path = data_path + '/training/' + session_name + "/output/classes.txt"
    f = OpenImageDataHelper(data_path)
    f.eval_model_OLD("/home/zulhakar/Downloads/cv/test", classes_path,
                     yolo=YOLO(model_path=model_path, classes_path=classes_path), show_images=True, save_images=True)


def eval_with_mAP_tool(session_name, data_path, h5_file):
    model_path = data_path + '/training/' + session_name + "/output/" + h5_file
    classes_path = data_path + '/training/' + session_name + "/output/classes.txt"
    f = OpenImageDataHelper(data_path)
    f.eval_model_with_map_tool("/home/zulhakar/Downloads/cv/test", classes_path,
                               yolo=YOLO(model_path=model_path, classes_path=classes_path), save_images=False)


session_name = "session1-man-woman"
data_path = '/home/zulhakar/Downloads/cv'
#training_with_person_body_parts(data_path, session_name, setup_train=True, training=True, max_images_per_class=3300, batchsize=30, input_shape=(416, 416), epochs=20)

session_name = "session1-man-woman_im_3300_shape_416_batch_30"
#session_name = 'session1'

# training_with_80_coco_classes(data_path, session_name, False, True)
#training_with_80_coco_classes(data_path, session_name, setup_train=True, training=True, max_images_per_class=1760, batchsize=32, input_shape=(320, 320), epochs=10)
#session_name = "session1_im_440_shape_320_batch_32"
# session_name = "yolov3_origin"
eval_with_mAP_tool(session_name, data_path, 'ep012-loss34.912-val_loss33.758.h5')
#eval_custom_test(session_name, data_path, 'ep009-loss25.789-val_loss25.615.h5')