# FigureEight DataPreProcessing

Es werden folgende files/dir von [FigureEightData](https://www.figure-eight.com/dataset/open-images-annotated-with-bounding-boxes/) benötigt:
Noch mehr [google](https://storage.googleapis.com/openimages/web/download.html)
```bash
├── data_dir
│   ├── class-descriptions-boxable.csv
│   ├── train-annotations-bbox.csv
│   └── train_XY
│       ├── c0a0a84c9586b7f2.jpg
│       └──....

```
class-descriptions-boxable.csv, train-annotations-bbox.csv,  train_00



## Usage

```python
import TODO

foobar.pluralize('word') # returns 'words'
foobar.pluralize('goose') # returns 'geese'
foobar.singularize('phenomena') # returns 'phenomenon'
```

## Created with

[makeareadme](https://www.makeareadme.com/#rendered-1)
