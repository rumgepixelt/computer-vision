import requests
import sys

def download_with_progress(link, file_name, target_dir=None):
    if target_dir is None:
        file_path = file_name
    else:
        file_path = target_dir + "/" + file_name

    with open(file_path, "wb") as f:
        print("Downloading %s" % file_name)
        response = requests.get(link, stream=True)
        total_length = response.headers.get('content-length')

        if total_length is None:  # no content length header
            f.write(response.content)
        else:
            dl = 0
            total_length = int(total_length)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                f.write(data)
                done = int(50 * dl / total_length)
                sys.stdout.write("\r[%s%s] %s / 100" % ('=' * done, ' ' * (50 - done), (done * 2)))
                sys.stdout.flush()
            print("\n")


