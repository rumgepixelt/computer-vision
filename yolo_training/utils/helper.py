import csv


def write_dict_to_csv(dict_data, column_names, target_file):
    try:
        with open(target_file, 'w') as csvfile:
            w = csv.writer(csvfile)
            w.writerows(dict_data.items())

    except IOError:
        print("I/O error")


def create_coco_to_google_map(google_class_names):
    synoym_map = {"motorbike": "Motorcycle", "aeroplane": "Airplane", "frisbee": "Flying disc", "skis": "Ski",
                  "sports ball": "Ball", "donut": "Doughnut", "pottedplant": "Houseplant",
                  "diningtable": "Kitchen & dining room table", "tvmonitor": "Television",
                  "cell phone": "Telephone", "hair drier": "Hair dryer", "oven": "Oven", "cow": "Bull",
                  "mouse": "Mouse", "clock": "Clock"}
    coco_to_google_dict = dict()
    with open("./coco.names") as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    in_google = set()
    for name in google_class_names:
        only_lower_case = name.lower()
        for name_coco in content:
            if name_coco in only_lower_case:
                in_google.add(name_coco)
            if name_coco in synoym_map.keys():
                in_google.add(name_coco)
    print(in_google)
    print(str(len(in_google)))
    for n in content:
        if n not in in_google:
            print(n)
    for coco_name in content:
        if coco_name in synoym_map.keys():
            coco_to_google_dict[coco_name] = synoym_map[coco_name]
            continue
        for google_name in google_class_names:
            google_name_lower = google_name.lower()
            if coco_name in google_name_lower:
                coco_to_google_dict[coco_name] = google_name
                break
    dict_to_list = list()
    print(str(len(coco_to_google_dict.keys())))
    for key in coco_to_google_dict.keys():
        print(key, ":", coco_to_google_dict[key])
        dict_to_list.append([key, coco_to_google_dict[key]])
    write_dict_to_csv(coco_to_google_dict, ["coco_names", "google_name"], "./coco_names_to_google.csv")
