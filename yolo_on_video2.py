# import the necessary packages
import numpy as np
import argparse
import imutils
import time
import cv2
import os
from PIL import Image
from yolo_training.yolo3.yolo import YOLO


# construct the argument parse and parse the arguments
def detect_labels(yolo, image):
    r_image, detect_annotations, scores = y.detect_classes(image)
    for i in range(0, len(detect_annotations)):
        # < class_name > < confidence > < left > < top > < right > < bottom >
        bboxB = (detect_annotations[i][1], detect_annotations[i][3], detect_annotations[i][2],
                 detect_annotations[i][4])
        label = detect_annotations[i][0]
        score = str(scores[i])


def init_yolo():
    session_name = "session1_im_440_shape_320_batch_32"
    data_path = "/home/zulhakar/Downloads/cv"
    h5_file = "ep009-loss25.789-val_loss25.615.h5"
    model_path = data_path + '/training/' + session_name + "/output/" + h5_file
    classes_path = data_path + '/training/' + session_name + "/output/classes.txt"
    yolo = YOLO(model_path=model_path, classes_path=classes_path)
    return yolo


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,
                help="path to input video")
ap.add_argument("-o", "--output", required=True,
                help="path to output video")
ap.add_argument("-y", "--yolo", required=True,
                help="base path to YOLO directory")
ap.add_argument("-c", "--confidence", type=float, default=0.5,
                help="minimum probability to filter weak detections")
ap.add_argument("-t", "--threshold", type=float, default=0.3,
                help="threshold when applyong non-maxima suppression")
args = vars(ap.parse_args())

# load the COCO class labels our YOLO model was trained on
labelsPath = os.path.join("/home/zulhakar/PycharmProjects/computer-vision/yolo_training/yolo3/model_data",
                          "coco_classes.txt")
LABELS = open(labelsPath).read().strip().split("\n")

# initialize a list of colors to represent each possible class label
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),
                           dtype="uint8")

# derive the paths to the YOLO weights and model configuration
weightsPath = os.path.join(
    "/home/zulhakar/PycharmProjects/computer-vision/yolo_training/yolo3/model_data/yolov3.weights")
configPath = os.path.join("/home/zulhakar/PycharmProjects/computer-vision/yolo_training/yolo3/yolov3.cfg")

# load our YOLO object detector trained on COCO dataset (80 classes)
# and determine only the *output* layer names that we need from YOLO
print("[INFO] loading YOLO from disk...")
net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
ln = net.getLayerNames()
ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

# initialize the video stream, pointer to output video file, and
# frame dimensions
vs = cv2.VideoCapture(args["input"])
writer = None
(W, H) = (None, None)

yolo = init_yolo()

# try to determine the total number of frames in the video file
try:
    prop = cv2.cv.CV_CAP_PROP_FRAME_COUNT if imutils.is_cv2() \
        else cv2.CAP_PROP_FRAME_COUNT
    total = int(vs.get(prop))
    print("[INFO] {} total frames in video".format(total))

# an error occurred while trying to determine the total
# number of frames in the video file
except:
    print("[INFO] could not determine # of frames in video")
    print("[INFO] no approx. completion time can be provided")
    total = -1

# loop over frames from the video file stream
while True:
    # read the next frame from the file
    (grabbed, frame) = vs.read()

    # if the frame was not grabbed, then we have reached the end
    # of the stream
    if not grabbed:
        break

    # if the frame dimensions are empty, grab them
    if W is None or H is None:
        (H, W) = frame.shape[:2]
    # construct a blob from the input frame and then perform a forward
    # pass of the YOLO object detector, giving us our bounding boxes
    # and associated probabilities
    #blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416),
          #                       swapRB=True, crop=False)
    #net.setInput(blob)
    # start = time.time()
    # layerOutputs = net.forward(ln)
    # end = time.time()
    cv2_im = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    pil_im = Image.fromarray(cv2_im)

    # initialize our lists of detected bounding boxes, confidences,
    # and class IDs, respectively
    boxes = []
    confidences = []
    classIDs = []
    r_image, detect_annotations, scores = yolo.detect_classes(pil_im)
    for i in range(0, len(detect_annotations)):
        # < class_name > < confidence > < left > < top > < right > < bottom >
        boxes.append((detect_annotations[i][1], detect_annotations[i][3], detect_annotations[i][2] - detect_annotations[i][1],
                      detect_annotations[i][4] - detect_annotations[i][3]))
        label = detect_annotations[i][0]
        confidences.append(scores[i])
        classIDs.append(detect_annotations[i][5])
    # loop over each of the layer outputs

    # apply non-maxima suppression to suppress weak, overlapping
    # bounding boxes
    for i in range(0,len(boxes)):
        # extract the bounding box coordinates
        (x, y) = (boxes[i][0], boxes[i][1])
        (w, h) = (boxes[i][2], boxes[i][3])

        # draw a bounding box rectangle and label on the frame
        color = [int(c) for c in COLORS[classIDs[i]]]
        cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
        text = "{}: {:.4f}".format(LABELS[classIDs[i]],
                                   confidences[i])
        cv2.putText(frame, text, (x, y - 5),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

    # check if the video writer is None
    if writer is None:
        # initialize our video writer
        fourcc = cv2.VideoWriter_fourcc(*"MJPG")
        writer = cv2.VideoWriter(args["output"], fourcc, 30,
                                 (frame.shape[1], frame.shape[0]), True)

        # some information on processing single frame

    # write the output frame to disk
    writer.write(frame)

# release the file pointers
print("[INFO] cleaning up...")
writer.release()
vs.release()
