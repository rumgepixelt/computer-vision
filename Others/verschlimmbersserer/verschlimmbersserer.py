

import cv2
import numpy as np
import os
from random import randint, choice

def increase_brightness(img, increase=30):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    v = img[:, :, 1]
    v = np.where(v <= 255 - increase, v + increase, 255)

    img[:, :, randint(0, img.shape[-1]-1)] = v

    img = cv2.cvtColor(img, cv2.COLOR_HSV2BGR)

    return img

def change_some_random_stuff(img, percent):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    for x0 in range(0, len(img)):
        img[x0] = recursive_stuff(img[x0], percent)

    img = cv2.cvtColor(img, cv2.COLOR_HSV2BGR)

    return img

def recursive_stuff(target, p):
    if type(target) is np.ndarray:
        for x in range(0, len(target)):
            target[x] = recursive_stuff(target[x], p)
    elif not p <= randint(0, 100):
        target = randint(0, 255)
    return target

def increase_brightness_random(img, value_min, value_max):
    value = randint(value_min, value_max)
    if choice([True, False]):
        value *= -1

    return increase_brightness(img, value)

def resize(img, size):


    h, b, v = img.shape
    temp = 1.0

    if h > b:
        if b > size:
            temp = size / b
    else:
        if h > size:
            temp = size / h

    h = int(temp * h)
    b = int(temp * b)
    temp = (b, h)

    img = cv2.resize(img, temp)

    return img

if __name__ == '__main__':

    # parameter aendern
    #################### -->

    random_noise_percent = 30
    min_random_brightness = 0
    max_random_brightness = 255


    input_dir = '/home/ubuntu-tower/Schreibtisch/testing/in/'
    out_dir = '/home/ubuntu-tower/Schreibtisch/testing/out/'

    name_marker = "_BAD_"

    # input_size = 416 # yolov3 resize to


    #################### <--

    max = len(os.listdir(input_dir))
    i = 0
    curr = -1

    for file in os.listdir(input_dir):
        i += 1
        if ((100/max) * i) > curr:
            curr = int(((100/max) * i))
            print(str(curr) + "%")
        try:
            img = cv2.imread(input_dir + file)  # load rgb image

            # img = resize(img, input_size)
        except:
            print("failed to load: " + input_dir + file)
            continue

        img = increase_brightness_random(img, 50, 200)
        img = change_some_random_stuff(img, random_noise_percent)

        ending = file[file.rfind("."):]
        file = file[:file.rfind(".")]

        try:
            cv2.imwrite(out_dir + file + name_marker + "." + ending, img)
        except:
            print("failed to save: " + out_dir + file + name_marker + "." + ending)

    print("done")
