import itertools
import xml.etree.ElementTree as ET
# import xml.etree.cElementTree as ET

with open('../data/facade_features/CMP_facade_DB_base/base/cmp_b0002.xml') as f:
    it = itertools.chain('<root>', f, '</root>')
    root = ET.fromstringlist(it)

for i in root:
    print(i[0][0].text)