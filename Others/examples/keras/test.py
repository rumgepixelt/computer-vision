from keras.applications.resnet50 import ResNet50
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions
import numpy as np
from os import listdir, path

model = ResNet50(weights='imagenet')

#img_path = '../../data/test_images/9674275407_59f996ffa1_z.jpg'
img_path = '/home/ubuntu/Schreibtisch/image-1434728-860_poster_16x9-ovyj-1434728.jpg'
# img = image.load_img(img_path, target_size=(224, 224))

img_dir = '../../data/test_images/'

image_list = list()
name_list = list()
if path.isdir(img_dir):
    for x in listdir(img_dir):
        if x.endswith(".jpg"):
            name_list.append(x)
            temp = img_dir + x
            temp = image.load_img(temp, target_size=(224, 224))

            temp = image.img_to_array(temp)
            temp = np.expand_dims(temp, axis=0)
            temp = preprocess_input(temp)
            image_list.append(temp)


# x = image.img_to_array(img)
# x = np.expand_dims(x, axis=0)
# x = preprocess_input(x)

for z in range(0, len(image_list)):
    x = image_list[z]
    y = name_list[z]
    preds = model.predict(x)
    # decode the results into a list of tuples (class, description, probability)
    # (one such list for each sample in the batch)
    print()
    print(y)
    print('Predicted:', decode_predictions(preds)[0])

    # Predicted: [(u'n02504013', u'Indian_elephant', 0.82658225), (u'n01871265', u'tusker', 0.1122357), (u'n02504458', u'African_elephant', 0.061040461)]