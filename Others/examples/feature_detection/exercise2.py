import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
import os
from os.path import isfile, join
path = "../books"
images_book = os.listdir(path)
input_image = None
for image in images_book:
    if "input" in image:
        input_image = cv.imread(path + "/" + image, 0)

# Initiate FAST object with default values
fast = cv.FastFeatureDetector_create()
# find and draw the keypoints
kp = fast.detect(input_image,None)
img2 = input_image.copy()
for marker in kp:
	img2 = cv.drawMarker(img2, tuple(int(i) for i in marker.pt), color=(0, 255, 0))

# Print all default params
print( "Threshold: {}".format(fast.getThreshold()) )
print( "nonmaxSuppression:{}".format(fast.getNonmaxSuppression()) )
print( "neighborhood: {}".format(fast.getType()) )
print( "Total Keypoints with nonmaxSuppression: {}".format(len(kp)) )
cv.imwrite('fast_true.png',img2)
# Disable nonmaxSuppression
fast.setNonmaxSuppression(0)
kp = fast.detect(input_image,None)
print( "Total Keypoints without nonmaxSuppression: {}".format(len(kp)) )
img3 = input_image.copy()
for marker in kp:
	img3 = cv.drawMarker(img2, tuple(int(i) for i in marker.pt), color=(0, 255, 0))
cv.imwrite('fast_false.png',img3)

